/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	function printUserInfo(){
		let username = prompt("Please enter your name.");
		let userAge = prompt("And your age?");
		let userLocation = prompt("What is your location?");
		alert("Thank you for your input!  (≖ᴗ≖)");

		console.log("(人 ◕ω◕) Hello, " + username + "!");
		console.log("(ᴗ ͜ʖ ᴗ) I understand that you are currently " + userAge + ".");
		console.log("( ・´з`・) And that you current location is " + userLocation + ".");
	};

	printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	let printFavBands = function myFavoriteBands(){
		console.log("( •̀ᴗ•́ )و Since you're here and all, allow me to geek out and tell you my favorite bands!");

		const band1 = "1. All Time Low";
		const band2 = "2. Gorillaz";
		const band3 = "3. Set It Off";
		const band4 = "4. Fall Out Boy";
		const band5 = "5. Saint Motel";

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);
	};

	printFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	let printFavMovies = function myFavoriteMovies(){
		console.log("( •̀ᴗ•́ )و Now here are my all time favorite movies!");

		const movie1 = "1. Puss In Boots: The Last Wish";
		const movie2 = "2. Spider-Man: Into the Spider-Verse";
		const movie3 = "3. del Toro's Pinocchio";
		const movie4 = "4. From Up on Poppy Hill";
		const movie5 = "5. About a Boy";

		const rTomatoRating = "🍅 Rotten Tomatoes Rating: ";

		console.log(movie1);
		console.log(rTomatoRating + "95%");
		console.log(movie2);
		console.log(rTomatoRating + "97%");
		console.log(movie3);
		console.log(rTomatoRating + "97%");
		console.log(movie4);
		console.log(rTomatoRating + "87%");
		console.log(movie5);
		console.log(rTomatoRating + "93%");
	};

	printFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends too. (⪩﹏⪨)");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("(〜^∇^)〜 You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};

	printFriends();
